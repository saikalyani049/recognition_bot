import os
import speech_recognition as sr
import tkinter as tk
from tkinter import filedialog

from code6 import save_text_to_database

def recognize_from_file(filename, callback):
    valid_file_name = filename.split("/")[-1].split(".")[0]
    print("Just File Name : ",valid_file_name)
    r = sr.Recognizer()
    with sr.AudioFile(filename) as source:
        audio_data = r.record(source)
        text = r.recognize_google(audio_data)
        callback(text, valid_file_name)

def save_text_to_file(text, filename, callback):
    text_files_folder = "textFiles"
    if not os.path.exists(text_files_folder):
        os.makedirs(text_files_folder)

    text_file_path = os.path.join(text_files_folder, f"{filename}.txt")
    with open(text_file_path, 'w') as f:
        f.write(text)

    print(f"Successfully saved {filename}.txt to the 'textFiles' folder.")
    if callback:
        callback(filename)

class VoiceText:
    def __init__(self, root):
        self.root = root
        self.frame1 = tk.Frame(root, bg='sky blue')
        self.frame1.pack(fill=tk.BOTH, expand=True)

        self.title_lbl = tk.Label(self.frame1, text='Voice Recognition Bot', font=('Times New Roman', '40', 'bold'), fg='maroon', bg='sky blue')
        self.title_lbl.place(x=200, y=5)

        label = tk.Label(self.frame1, text="Select a file:", bg='sky blue', font=('ariel', '12'))
        label.place(x=200, y=124)

        self.entry_box = tk.Entry(self.frame1, width=50)
        self.entry_box.place(x=300, y=125)

        browse_button = tk.Button(self.frame1, text="Browse", command=self.browse_file)
        browse_button.place(x=420, y=170)

        self.submit_btn = tk.Button(self.frame1, text='Submit', command=self.submit_file)
        self.submit_btn.place(x=420, y=220)

        self.frame2 = tk.Frame(root, bg='white')
        self.frame2.pack_forget()

        self.output_label = tk.Label(self.frame2, text='', font=('ariel', '12'), bg='white')
        self.output_label.pack(pady=20)

        self.back_button = tk.Button(self.frame2, text='Back', command=self.back_to_frame1)
        self.back_button.pack(pady=20)

        self.save_button = tk.Button(self.frame2, text='Save', command=lambda: save_text_to_file(self.output_label.cget('text'), self.valid_file_name, self.save_success_callback))
        self.save_button.pack(pady=20)

    def browse_file(self):
        file_path = filedialog.askopenfilename()
        self.entry_box.delete(0, tk.END)
        self.entry_box.insert(0, file_path)

    def submit_file(self):
        file_path = self.entry_box.get()
        print("File Path Printing : ",file_path)
        if file_path:
            recognize_from_file(file_path, self.show_output)
        else:
            print("No file selected")

    def show_output(self, text, valid_file_name):
        self.valid_file_name = valid_file_name
        self.frame1.pack_forget()
        self.frame2.pack(fill=tk.BOTH,expand=True)
        self.output_label.config(text=text, wraplength=400)

    def back_to_frame1(self):
        self.frame2.pack_forget()
        self.frame1.pack(fill=tk.BOTH, expand=True)

    def save_success_callback(self, filename):
        message_label = tk.Label(self.frame2, text=f"Successfully saved {filename}", font=('ariel', '12'), bg='white')
        message_label.pack(pady=20)
        save_text_to_database(self.output_label.cget('text'), self.valid_file_name)

root = tk.Tk()
root.title('voice bot')
root.geometry('900x400+300+80')
root.config(background='sky blue')

vt = VoiceText(root)

root.mainloop()
